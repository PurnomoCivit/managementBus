@extends('layouts.app')

@section('content')
    <a href="{{ route('bus.create') }}" class="btn btn-info btn-sm">bus Baru</a>
    
    @if ($message = Session::get('message'))
        <div class="alert alert-success martop-sm">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-responsive martop-sm">
        <thead>
            <th>ID</th>
            <th>Nama bus</th>
            <th>Action</th>
        </thead>
        <tbody>
            @foreach ($bus as $busWilayah)
                <tr>
                    <td>{{ $busWilayah->id }}</td>
                    <td><a href="{{ route('bus.show', $busWilayah->id) }}">{{ $busWilayah->bus_name }}</a></td>
                    <td>
                        <form action="{{ route('bus.destroy', $busWilayah->id) }}" method="post">
                            {{csrf_field()}}
                            {{ method_field('DELETE') }}
                            <a href="{{ route('bus.edit', $busWilayah->id) }}" class="btn btn-warning btn-sm">Ubah</a>
                            <button type="submit" class="btn btn-danger btn-sm">Hapus</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection