@extends('layouts.app')

@section('content')
<h4>Ubah bus</h4>
<form action="{{ route('bus.update', $bus->id) }}" method="post">
    {{csrf_field()}}
    {{ method_field('PUT') }}
    <div class="form-group row {{ $errors->has('bus_name') ? 'has-error' : '' }}">
        <label for="bus_name" class="col-sm-2 col-form-label">Nama bus</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="bus_name" placeholder="Nama bus" value="{{ $bus->bus_name }}">
        </div>
        @if ($errors->has('bus_name'))
            <span class="help-block">{{ $errors->first('bus_name') }}</span>
        @endif
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-info">Simpan</button>
        <a href="{{ route('bus.index') }}" class="btn btn-default">Kembali</a>
    </div>
</form>
@endsection