@extends('layouts.app')

@section('content')
<h4>Beli Tiket Bis Online</h4> 

<form action="{{ url('search') }}" method="get">
	<div class="form-group row">
		<div class="col-sm-4">
			<label class="col-sm-12">Kota Keberangkatan</label>
			<select name="keberangkatan" class="selectpicker col-sm-12" data-live-search="true">
				@foreach ($provinsi as $provinsiWilayah)
					<optgroup label="{{ $provinsiWilayah->provinsi_name}}">
						@foreach ($provinsiWilayah->kota as $city)
							<option data-tokens="{{ $provinsiWilayah->provinsi_name}}, {{ $city->kota_nama }}" value="{{$city->kode_kota}}">{{ $provinsiWilayah->provinsi_name}} - {{ $city->kota_nama }}</option>
						@endforeach
					</optgroup>
				@endforeach
			</select>
		</div>
		<div class="col-sm-4">
			<label class="col-sm-12">Kota Tujuan</label>
			<select name="tujuan" class="selectpicker col-sm-12" data-live-search="true">
				@foreach ($provinsi as $provinsiWilayah)
					<optgroup label="{{ $provinsiWilayah->provinsi_name}}">
						@foreach ($provinsiWilayah->kota as $city)
							<option data-tokens="{{ $city->kota_nama }}" value="{{$city->kode_kota}}">{{ $provinsiWilayah->provinsi_name}} - {{ $city->kota_nama }}</option>
						@endforeach
					</optgroup>
				@endforeach
			</select>
		</div>	

		<div class="col-sm-4"></div>
	</div>
	<div class="form-group row">
		<div class="col-sm-4">
			<input type="date" class="form-control" name="date" />
		</div>
		<div class="col-sm-4">
			<div class="form-group">
		        <button type="submit" class="btn btn-info">Cari Jadwal</button>
		    </div>
		</div>
	</div>
</form>
<!-- <a href="{{ route('provinsi.index') }}" class="btn btn-default">Kembali</a> -->
@endsection