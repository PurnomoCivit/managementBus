@extends('layouts.app')

@section('content')
<h4>route Baru</h4>
<form action="{{ route('route.store') }}" method="post">
    {{csrf_field()}}
    <div class="form-group row {{ $errors->has('route_name') ? 'has-error' : '' }}">
        <label for="title" class="col-sm-2 col-form-label">Nama route</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="route_name" placeholder="Nama route">
        </div>
        @if ($errors->has('route_name'))
            <span class="help-block">{{ $errors->first('route_name') }}</span>
        @endif
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-info">Simpan</button>
        <a href="{{ route('route.index') }}" class="btn btn-default">Kembali</a>
    </div>
</form>
@endsection
