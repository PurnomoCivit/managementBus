@extends('layouts.app')

@section('content')
<h5>{{ $kota->provinsi->provinsi_name }}</h5>
<h4>{{ $kota->kota_nama }}</h4>
<a href="{{ route('kota.index') }}" class="btn btn-default">Kembali</a>
@endsection