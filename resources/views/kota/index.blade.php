@extends('layouts.app')

@section('content')
    <a href="{{ route('kota.create') }}" class="btn btn-info btn-sm">Kota Baru</a>
    
    @if ($message = Session::get('message'))
        <div class="alert alert-success martop-sm">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-responsive martop-sm">
        <thead>
            <th>ID</th>
            <th>Nama Provinsi</th>
            <th>Nama Kota</th>
            <th>Kode Kota</th>
            <th>Action</th>
        </thead>
        <tbody>
            @foreach ($kota as $kotaWilayah)
                <tr>
                    <td>{{ $kotaWilayah->id }}</td>
                    <td>{{ $kotaWilayah->provinsi->provinsi_name }}</td>
                    <td><a href="{{ route('kota.show', $kotaWilayah->id) }}">{{ $kotaWilayah->kota_nama }}</a></td>
                    <td>{{ $kotaWilayah->kode_kota }}</td>
                    <td>
                        <form action="{{ route('kota.destroy', $kotaWilayah->id) }}" method="post">
                            {{csrf_field()}}
                            {{ method_field('DELETE') }}
                            <a href="{{ route('kota.edit', $kotaWilayah->id) }}" class="btn btn-warning btn-sm">Ubah</a>
                            <button type="submit" class="btn btn-danger btn-sm">Hapus</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection