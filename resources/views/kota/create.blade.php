@extends('layouts.app')

@section('content')
<h4>Kota Baru</h4>
<form action="{{ route('kota.store') }}" method="post">
    {{csrf_field()}}
    <div class="form-group row {{ $errors->has('kode_kota') ? 'has-error' : '' }}">
        <label for="kode_kota" class="col-sm-2 col-form-label">Kode Kota*</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="kode_kota" placeholder="Kode Kota" required="">
        </div>
        @if ($errors->has('kode_kota'))
            <span class="help-block">{{ $errors->first('kode_kota') }}</span>
        @endif
    </div>
    <div class="form-group row {{ $errors->has('id') ? 'has-error' : '' }}">
        <label for="id" class="col-sm-2 col-form-label">Name Provinsi</label>
        <div class="col-sm-4">
            <select class="form-control" name="provinsi_id">
                @foreach ($provinsi as $provinsiWilayah)
                    <option value="{{ $provinsiWilayah->id }}">{{ $provinsiWilayah->provinsi_name }} </option>
                @endforeach
            </select>
        </div>
        @if ($errors->has('provinsi_id'))
            <span class="help-block">{{ $errors->first('provinsi_id') }}</span>
        @endif
    </div>
    <div class="form-group row {{ $errors->has('kota_nama') ? 'has-error' : '' }}">
        <label for="title" class="col-sm-2 col-form-label">Nama kota</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="kota_nama" placeholder="Nama kota">
        </div>
        @if ($errors->has('kota_nama'))
            <span class="help-block">{{ $errors->first('kota_nama') }}</span>
        @endif
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-info">Simpan</button>
        <a href="{{ route('kota.index') }}" class="btn btn-default">Kembali</a>
    </div>
</form>
@endsection
