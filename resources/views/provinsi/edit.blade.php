@extends('layouts.app')

@section('content')
<h4>Ubah Provinsi</h4>
<form action="{{ route('provinsi.update', $provinsi->id) }}" method="post">
    {{csrf_field()}}
    {{ method_field('PUT') }}
    <div class="form-group row {{ $errors->has('provinsi_name') ? 'has-error' : '' }}">
        <label for="provinsi_name" class="col-sm-2 col-form-label">Nama Provinsi</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="provinsi_name" placeholder="Nama Provinsi" value="{{ $provinsi->provinsi_name }}">
        </div>
        @if ($errors->has('provinsi_name'))
            <span class="help-block">{{ $errors->first('provinsi_name') }}</span>
        @endif
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-info">Simpan</button>
        <a href="{{ route('provinsi.index') }}" class="btn btn-default">Kembali</a>
    </div>
</form>
@endsection