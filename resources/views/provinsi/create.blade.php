@extends('layouts.app')

@section('content')
<h4>Provinsi Baru</h4> 
<form action="{{ route('provinsi.store') }}" method="post">
    {{csrf_field()}}
    <div class="form-group row {{ $errors->has('provinsi_name') ? 'has-error' : '' }}">
        <label for="title" class="col-sm-2 col-form-label">Nama Provinsi</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="provinsi_name" placeholder="Nama Provinsi">
        </div>
        @if ($errors->has('provinsi_name'))
            <span class="help-block">{{ $errors->first('provinsi_name') }}</span>
        @endif
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-info">Simpan</button>
        <a href="{{ route('provinsi.index') }}" class="btn btn-default">Kembali</a>
    </div>
</form>
@endsection
