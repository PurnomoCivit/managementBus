<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusType extends Model
{
    protected $table = 'bus_type';
    protected $fillable = [
        'id', 'bus_type_name', 'created_at', 'created_by', 'updated_at', 'updated_by',
    ];

    public function bus()
    {
    	returm $this->hasMany("App\Bus");
    }
}
