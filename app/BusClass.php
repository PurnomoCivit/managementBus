<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusClass extends Model
{
    protected $table = 'bus_class';
    protected $fillable = [
        'id', 'bus_class_name', 'created_at', 'created_by', 'updated_at', 'updated_by',
    ];

    public function bus()
    {
    	returm $this->hasMany("App\Bus");
    }
}
