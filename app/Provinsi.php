<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    protected $table = 'provinsi';
    protected $fillable = [
        'id', 'provinsi_name', 'created_at', 'created_by', 'updated_at', 'updated_by',
    ];

    public function kota()
    {
    	return $this->hasMany("App\Kota");
    }
}
