<?php

namespace App\Http\Controllers;

use App\Kota;
use App\Provinsi;
use Illuminate\Http\Request;

class KotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $kota = Kota::orderBy('id', 'DESC')->with("provinsi")->paginate(5);
        return view('kota.index', compact('kota'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $provinsi = Provinsi::orderBy('provinsi_name', 'ASC')->get();
        return view('kota.create', compact('provinsi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'kode_kota' => 'required',
            'provinsi_id' => 'required',
            'kota_nama' => 'required',
        ]);

        $kota = Kota::create($request->all());

        return redirect()->route('kota.index')->with('message', 'Kota berhasil dibuat!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $kota = Kota::with('provinsi')->findOrFail($id);

        return view('kota.show', compact('kota'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $kota = Kota::findOrFail($id);
        $provinsi = Provinsi::orderBy('provinsi_name', 'ASC')->get();
        return view('kota.edit', compact('kota', 'provinsi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'kota_nama' => 'required'
        ]);

        $kota = Kota::findOrFail($id)->update($request->all());

        return redirect()->route('kota.index')->with('message', 'kota berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $kota = Kota::findOrFail($id)->delete();
        return redirect()->route('kota.index')->with('message', 'kota berhasil dihapus!');
    }
}
