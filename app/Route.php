<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
	protected $table = 'route';
    protected $fillable = [
        'id', 'bus_id', 'route_city_fron', 'route_city_to', 'take_off_time', 'arrive_time', 'price', 'created_at', 'created_by', 'updated_at', 'updated_by',
    ];

    public function bus()
    {
    	return $this->belongsTo("App\Bus");
    }
}
