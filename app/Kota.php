<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kota extends Model
{
    protected $table = 'kota';
    protected $fillable = [
        'id', 'provinsi_id', 'kota_nama', 'kode_kota', 'created_at', 'created_by', 'updated_at', 'updated_by',
    ];

    public function provinsi()
    {
    	return $this->belongsTo("App\Provinsi");
    }
}
