<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bus extends Model
{
    protected $table = 'bus';
    protected $fillable = [
        'id', 'bus_type_id', 'bus_class_id', 'bus_name', 'bus_code', 'created_at', 'created_by', 'updated_at', 'updated_by',
    ];

    public function busType()
    {
    	return $this->belongsTo("App\BusType");
    }

    public function busClass()
    {
    	return $this->hasOne("App\BusClass");
    }

    public function Route()
    {
    	return $this->hasMany("App\Route");
    }
}
