<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusClassTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bus_class', function (Blueprint $table) {
            $table->autoIncrement('id', 11);
            $table->string('bus_class_name', 25);
            $table->datetime('created_at');
            $table->int('created_by', 11);
            $table->datetime('updated_at');
            $table->int('updated_by', 11);
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bus_class');
    }
}
