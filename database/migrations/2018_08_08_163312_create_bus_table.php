<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bus', function (Blueprint $table) {
            $table->autoIncrement('id', 11);
            $table->int('bus_type_id', 11);
            $table->int('bus_class_id', 11);
            $table->string('bus_name', 25);
            $table->string('bus_code', 15);
            $table->datetime('created_at');
            $table->int('created_by', 11);
            $table->datetime('updated_at');
            $table->int('updated_by', 11);
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bus');
    }
}
