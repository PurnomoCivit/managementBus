<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKotaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kota', function (Blueprint $table) {
            $table->int('id', 11);
            $table->int('provinsi_id', 11);
            $table->string('kota_nama', 25);
            $table->string('kode_kota', 15);
            $table->datetime('created_at');
            $table->int('created_by', 11);
            $table->datetime('updated_at');
            $table->int('updated_by', 11);
            $table->primary('kota_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kota');
    }
}
