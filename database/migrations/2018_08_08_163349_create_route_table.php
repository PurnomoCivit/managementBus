<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRouteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('route', function (Blueprint $table) {
            $table->autoIncrement('id', 11);
            $table->int('bus_id', 11);
            $table->int('route_city_from', 11);
            $table->int('route_city_to', 11);
            $table->time('take_off_time');
            $table->time('arrive_time');
            $table->int('price');
            $table->datetime('created_at');
            $table->int('created_by', 11);
            $table->datetime('updated_at');
            $table->int('updated_by', 11);
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('route');
    }
}
