<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvinsiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provinsi', function (Blueprint $table) {
            $table->autoIncrement('id', 11);
            $table->string('provinsi_name', 25);
            $table->datetime('created_at');
            $table->int('created_by', 11);
            $table->datetime('updated_at');
            $table->int('updated_by', 11);
            $table->primary('provinsi_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provinsi');
    }
}
