# managementBus
Project ini dibuat menggunakan Laravel Framework and use PHP v7.1


[![Latest Stable Version](https://poser.pugx.org/laravel/laravel/v/stable)](https://packagist.org/packages/laravel/laravel)
[![Total Downloads](https://poser.pugx.org/laravel/laravel/downloads)](https://packagist.org/packages/laravel/laravel)

## Prerequisites

````
1. Install Composer
````
## Installing

````
1. git clone https://gitlab.com/PurnomoCivit/managementBus.git name_project_folder

2. change database setting on database file. Location : .env

3. open your terminal and sign in to your project forlder :

   php artisan migrate


4. after that run your project :
   php artisan server
````
### Running the tests

````
Open Browser and type the url start with http://127.0.0.1:8000/
````

